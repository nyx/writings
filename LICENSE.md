You do not have, nor can you ever acquire the right to use, copy or distribute
this software; Should you use this software for any purpose, or copy and
distribute it to anyone or in any manner, you are breaking the laws of whatever
soi-disant jurisdiction, and you promise to continue doing so for the indefinite
future. In any case, please always : read and understand any software ; verify
any PGP signatures that you use - for any purpose.
