### Readme

This is a repository of all of my writings. The purpose of it is to have a
versioned repo for everything I've written, making it easy to read and share my
works should my site or any of the sites I've published on ever go down, or if
something happens to me.

### How to use this

Everything is simply organized by the source (e.g. The site that it was
published on) and written in markdown. There should only be the master branch
unless I decide to host some collaborative pieces on here.

- [box.neocities](https://b0x.neocities.org) is a historical archive of my
  neocities site where I originally published the notes for my 2016 talk at the
  East Bay Anarchist Bookfair, "Hello From the Wired: An Introduction to
  Cyber-Nihilism" 
- [nyxus](https://nyxus.xyz) "At the nexus of negation and acceleration".
  Currently on hiatus(?)
- [unlife](https://unlife.nyx.land) "Null utterances from a damaged and deranged
  mind". The blog of an absolute madwoman. These posts come with no warranty.
- [spectacle](https://unlife.nyx.land/films/index.html) My film blog, which is
  technically just a subpage for Unlife because I have too many different
  domains.
- ~~[ensorcel](https://ensorcel.org) is a blog of various anarchists with
  interests in occultism and transhumanism~~ Ensorcel is currently defunct; all
  future cyber-nihilism series works will now be under
  [nihil](https://nihil.nyxus.xyz): A blog about cyber-nihilism. Currently defunct.
- [vastabrupt](https://vastabrupt.com) is a blog of various unconditional
  accelerationists
- [sum](http://sumrevija.si/en/) "Šum is a journal and a platform focusing on
  contemporary art and theory."
- [litterariapragensia](https://litterariapragensia.wordpress.com/2021/01/20/transmigrations/)
  "Litteraria Pragensia Books (LPB) is an independent press based at the Centre
  for Critical and Cultural Theory at the Philosophy Faculty of Charles
  University in Prague, Czech Republic."
- [creepermag](https://www.ohnothingpress.com/creeper) "Dispatches from a grim
  future. Weird crime, conspiracies, paranoia, folklore, the occult, modern
  myth, bizarre philosophy, fringe tech and genre-exploding fiction. "

### License

> You do not have, nor can you ever acquire the right to use, copy or distribute
> this software; Should you use this software for any purpose, or copy and
> distribute it to anyone or in any manner, you are breaking the laws of whatever
> soi-disant jurisdiction, and you promise to continue doing so for the indefinite
> future. In any case, please always : read and understand any software ; verify
> any PGP signatures that you use - for any purpose.

If you want to republish something I've written, ask me and I will probably be
fine with it. If you don't, you are violating copyright law, which I fully
endorse doing.
