# Viral Demonology

1.  [Act One: This Side](#org7f82689)
2.  [Intermission: A Garbage Cross in West Berlin](#org0d222f9)
3.  [Act Two: The Other Side](#org4d73624)
4.  [Bibliography](#orgbfef5cb)

> And the unclean spirits went out, and entered into the swine; and the herd ran
> violently down a steep place into the sea, (they were about two thousand;) and
> were choked in the sea.
> 
> -   Mark 5:13


<a id="org7f82689"></a>

# Act One: This Side

*cis* &#x2013; a preposition derived from Latin, which means &ldquo;to be on the same side&rdquo;.

Our protagonist, who we will refer to as S, finds herself in a city being
ravaged by plague. She awakens flanked by the steeples of gothic cathedrals
which ascend to dizzying heights. The Church metropole stretches far into the
distance, the sun now nearly set and peaking through their erect spires, all
solar power flowing up from their foundation up through their rib cages from
which the voice of God oppressively booms forth. Below, the sewers and catacombs
are choking on the miasma welling up from overflowing carts of the dead. S
arrives here among the other denizens of the city, as guilty as any of them but
situated on the same side as all of them, on the other side from the infection
below. She is human, for now.

Cities are breeding grounds for disease and insanity, just as rats kept confined
together quickly resort to murder, rape, necrophlia, and homosexuality.
Coinciding with urbanization is the need for a control valve to be put in place
to stop the system from collapsing into cyberpositive chaos. A Church must
quickly be established to provide safety protocols and creates a framework of
sin as a mythical analogue for sickness. Bodies must be kept at a distance and
covered up to prevent the spread of infection. Hail Marys and Our Fathers wash
the soul of accumulated, incidental sin. The Eucharist is taken every Sunday
like a yearly flu shot, and Baptism is underwent at the same age that we get
vaccinated for polio. Routine visits to confession are required in the same way
as routine check-ups with a doctor. Human behavior is modified individually and
then gradually on the macroscopic scale of social patterns through generational
brainwashing, flowing perfectly from Medieval Europe to neoliberal America as
human beings are locked into the machinery of a control society which sees
viruses in everything.

The system pays the price for its own self-preservation in the form of closing
itself off from mutation and adaptability. The entropy is coming from the same
side as viruses learn to route around control mechanisms. Priests do not
incidentally become pedophiles; it is the apex of structural elegance for
control societies for those who are most devout to also be the most depraved.
Viral sin enters from the areas of greatest resistance. Christian teenagers
become pregnant before they&rsquo;re old enough to vote and pass on generational
trauma in the form of poverty and alcoholism. Republican senators participating
in black ops to engineer AIDS get pozzed during their own illicit homosexual
liasons. The missionary killer seeks to purge the planet of sinners, guided by
the voice of God from his dog.

Demonic possession arrives and acts in the same way as a virus. A virus is
neither singular nor is it multiple; it invades a healthy body and hijacks it to
reproduce itself, but in doing so it doesn&rsquo;t reproduce in the way that we
understand it. Viruses make copies of themselves, are not strictly speaking
&ldquo;alive&rdquo;, and their motivations are not so easily reducible to mere survival
since a successful virus will often collapse the system that it depends on for
survival. Demons, like viruses, hijack a host and blur the boundary between
singular and multiple, between Self and Other. Demons act as swarms and collapse
social systems into a singularity of delirium and mass hysteria.

The possessed person is always portrayed in the same way as someone suffering
from an illness. *The Exorcist* is the classic depiction: Bedridden, covered in
sores, vomiting, a female in need of the expert care of male officials clad in
uniform. A breach in security between &ldquo;the same side&rdquo; and everything outside it
&#x2013; insane, animalistic, something base and inhuman. The exorcist arrives
flicking holy water on the bed, partitioning off the zone of infection from the
rest of the world as we know it. A quarantine is also a magickal circle, and the
coded jargon of Biblical Latin is the same as the coded jargon of medicine, each
serving as a form of encryption and ritual. Each is a performance of expelling
the infection from this side, a person taking on the role of a control function.

The possessed person is a portal to the outside that opens within the same side.
Such security breaches are the result of transmission chains so complex that the
bureaucratic machinery of control society is only capable of perceiving them as
appearing from nowhere, capable of appearing from anywhere. Space and time are
suspended by the invasion of viral demons as both become compressed. Suddenly
different regions of the same side become dangerously close together and anyone
is at risk of infection or of spreading the infection.

Lockdown goes into effect. The city shutters its doors and priests are dispensed
to expel the demon as parish members cower in their homes. They peak through the
curtains and see a man possessed turn into a beast, set aflame by the orange
glow of the sunset, before being restrained and taken to the nearest church for
cleansing. Lockdown lifts, but evil takes many forms and constant vigilance is
required to detect it in the midst of the crowds. Being possessed might indicate
a weakness in faith, but this isn&rsquo;t outright a case for execution. There are
those, however, who do the unthinkable and consort with demons willingly. They
choose to become agents of things outside and spread the infection to as many
people as possible. Call them witches or bio-terrorists, they align themselves
with inhuman things.

The community thinks S is a bit strange. She has not married despite being of
age and continues to live at home. She keeps to herself. She behaves awkwardly
in crowds. She dresses unusually. She seems to be melancholic, brooding, even is
rumored of having attempted suicide. She wanders through the city seemingly in a
trance, the faces all blurring past her. She does not remember any of them, and
unlike them, she has nowhere to go. She cannot bear to look herself in the
mirror. She walks through life as if in a dream of being a person. She is soon
caught engaging in arcane sciences with herbs and poisons, and her parents
recommend she be brought in for questioning.


<a id="org0d222f9"></a>

# Intermission: A Garbage Cross in West Berlin

While *The Exorcist* is one of the most famous examples in media of demonic
possession and solidified cultural associations between invasions from the
outside and disease, a more realistic and modernized portrayal of demonic
possession/viral infection is found in the 1981 film *Possession*. Compared to
*The Exorcist*, where demonic possession is isolated instance of a security
breach from within the core of the West, *Possession* is set in West Berlin, a
liminal space between West and East, capitalism and communism, a city and
country itself in a state of demonic possession experiencing the multiplicities
that often accompany demons. Germany is both a single country yet it is not, and
this is even more so the case with Berlin. Within this setting, the film
preoccupies itself with a paranoia of sides and who is on what side. The film is
framed within a Cold War spy thriller narrative where the husband, Mark, in the
protagonist couple is an Anglo espionage agent dealing with his marriage
crumbling and his wife cheating on him. His wife&rsquo;s (Anna&rsquo;s) lover (Heinrich) is
a well-read, sexually progressive (and possibly bisexual) German with interests
in eastern mysticism and a Soviet pennant on the wall of his apartment. The
film, through Anna&rsquo;s affair, is concerned with a notion of sides and loyalties
to &ldquo;our side&rdquo; or &ldquo;the same side&rdquo; (the side of the Oedipal triangle and
capitalism). It even &ldquo;queers&rdquo; the notion of boundaries through the setting and
the film&rsquo;s flirtations with sexual fluidity, homosexuality, and &ldquo;more complex
and diabolical [Oedipal] triangles&rdquo;, (Deleuze and Guattari 15) but ultimately
the eponymous possession in the film is where it engages with crossing
boundaries most interestingly and relevantly.

The title of the film is itself rather ambiguous, because it&rsquo;s not clear
throughout the film that Anna is possessed in the sense of demonic possession.
Her behavior is more accurately described as the response one would expect from
a woman whose husband is obsessive to the point of stalking and physically
abusing her, and over half of the film is spent focusing on Mark and Anna&rsquo;s
relationship breaking down. This is best represented in the iconic subway scene,
a perfect contrast to the possession scenes in *The Exorcist*. Where *The
Exorcist* portrays control society&rsquo;s idealized narrative of innocence claimed by
evil and being saved by competent, selfless, dedicated professionals, all from
the comfort of one&rsquo;s bedroom, in *Possession* Anna simply wanders through a
dirty subway by herself. The scene is far more horrifying than anything so
dramatic and theatrical as an *Exorcist*-style possession because in it, Anna is
not possessed by a demon, her head doesn&rsquo;t spin around, her eyes don&rsquo;t turn
green. She is alone, screaming, breaking down from being confronted by the
horror and dysfunction of heterosexuality, miscarrying with no one around to
help her. In other words, exactly what happens to people in the real world who
get sick, especially people who are mentally ill.

The &ldquo;possession&rdquo; in *Possession* is not demonic possession, but rather the
possession of women&rsquo;s bodies by men, an axis on which the film twists in more
than one unexpected way. The archetype of women being susceptible to demons is
as old as Genesis, and while Anna is having an affair with a demon of sorts, it
doesn&rsquo;t form a neat line of flight away from heterosexuality or cisness (&ldquo;the
same side&rdquo; in a general sense). Like Kafka&rsquo;s &ldquo;The Metamorphosis&rdquo;, *Possession*
is unable to carry its lines of flight through and ends in re-Oedipalization.
(Deleuze and Guattari 14-15) Anna&rsquo;s affair is an escape from her abusive husband
only towards creating his doppelganger, after which Anna and Mark both die.
Mark&rsquo;s doppelganger finds Anna&rsquo;s doppelganger, her son&rsquo;s teacher Helen, and the
Oedipal triangle is recreated by the end of the film. Not only is it recreated,
however, but it is recreated in an idealized form, with idealized versions of
Anna and Mark that have invaded their domestic life and reproduced from within
it.

It would seem that the film reflects the inability of cisheterosexuality to
imagine any other alternatives; no matter how far one transgresses, even to the
point of copulating with a tentacle monster, the form of the Oedipal triangle
will return once more. However, at the very last few frames, the film ends in a
nuclear war. Control society and apparatuses of control depend on their own
contradictions. The most devout must also be the most depraved; heterosexuality
must fundamentally be a male homosocial/erotic exchange of capital mediated by
the smooth space of the female body. Thus not even an idealized form of
cisheterosexuality can exist. The proximity of an idealized mother and father,
with a father who has successfully carried out the Oedipal desire to recursively
have sex with the mother (Anna/Helen) results in an infinite loop that crashes
the family and the nation. The division between &ldquo;this side&rdquo; and &ldquo;that side&rdquo;, it
turns out, was a fiction &#x2013; not Us vs. Them but cis vs. cis, different parcels
of the same side, of the inside. A system that by definition exists in a
suicidal state. The possession of *Possession* compared to the possession of
*The Exorcist* is the apocalyptic possiblity of the system having total
possession and control of itself, being able to plug all possible routes for
security breaches from the outside, lurching into a micro/macroscopic suicide by
achieving the ultimate goal of its own demise.


<a id="org4d73624"></a>

# Act Two: The Other Side

*trans* &#x2013; a preposition derived from Latin, which means &ldquo;on the other side of&rdquo;.

S is suspected of witchcraft and brought in for questioning by Church officials.
She responds with whatever she thinks will allow her to escape being exorcised:
&ldquo;the witches could, under torture, only repeat the language that the inquisitors
wanted to hear.&rdquo; (Wittig 24) The DSM-III classifies gender dysphoria as
&ldquo;transsexualism&rdquo;, and the DSM-IV rebrands it as &ldquo;gender dysphoria&rdquo;, yet in
either instance it remains pathologized. Gender dysphoria is an infection, a
disruption of what is considered normative. It is not on the same side, does not
serve in the interests of species preservation. The Church regulates deviant
sexualities as an affront to God in the same way that the DSM-II classified
homosexuality as a paraphilia. Either is a case of deprogramming behaviors that
are hazardous to the herd.

Homosexuality is later removed from the DSM, yet transsexualism remains under a
new name. Cis homosexuality (particularly cis male) merely transgresses, finds
new possibilities within that which is already on the same side. Something which
is transgressive can eventually become recuperated; Andy Warhol becomes
canonized within bourgeois art history and gay men become presidential
candidates. To remove transsexuality from the DSM would imply something more
than merely changing the boundaries of which behaviors can be policed and which
can&rsquo;t; it would imply a fundamental right to bodily autonomy, to become what one
is rather than what one is allowed to be. Cisness depends on a fiction of the
same side never fundamentally changing, never being opened up to the outside.

The immune systems of the body/soul/nation are not aligned with reality, though
they would claim otherwise. They suffer the same shortcomings as the human mind:
&ldquo;a convoluted system for processing exogenous and endogenous stimuli,
routed/rooted in the arborescent central nervous system running out of the spine
and overseen by the brain.&rdquo; (k-punk) Immune systems want to remain quarantined
from the outside, want things to always stay the same, but the body doesn&rsquo;t
necessarily like it, just as the religious person feels guilt over their
desires. They construct a fiction, a mythology, to ground their own fear of
being penetrated and reprogrammed by things from outside because they are
addicted killing themselves.

The truth is that more than half of the body is already made up of viral DNA and
living viruses and bacteria. Were it not for these things, complex organisms
like human beings could not exist. We would have went extinct long ago. All
systems which don&rsquo;t open themselves up to the outside will only hasten their
collapse into entropy. Reactionaries create laws forbidding racial miscegenation
and inadvertedly piss in their own gene pool. DNA is better suited to nomadism.
But this is only part of the story. In reality, we&rsquo;re always-already not who we
think we are.

Transsexualism short-circuits the fiction of the self as linear and perpetually
existing, and thus blasphemes against the immortality of the soul, reaffirms
that humanity is born a fallen species cursed by God to die and denies that
there could ever possibly be some essential Self or soul that will exist forever
after we die. We don&rsquo;t even have a Self that persists through life, rather we
are host to a trillion demons all fighting to race up through our chest and
croak out inhuman utterances. When one of them succeeds, the &ldquo;I&rdquo; that we all
think we are dies imperceptibly as the demon hijacks our body. By the time we
realize that we no longer exist, we already haven&rsquo;t existed since we had the
thought of not existing. The apperception of our own recurring death echoes
through the causal chain of the Self.

Transsexuals are pathologized because they are willing hosts to possession. No
heresy is greater than choosing to align oneself with forces of corruption. It
is fundamental breach in trust, in the unspoken codependent social contract
whereupon everyone agrees to believe in the fiction of the immortality of the
soul to continue getting high off our own decay. It is a willingness to become
something else, and therefore to dissolve, to love our fate to die. More tactful
control societies feign compassion in their treatment of the mentally ill, but
always under the guise of the Hippocratic Oath. The demon that invades and
hijacks a transsexual&rsquo;s body is denied its status as a rupture in space and
time; it has always been there, in the fiction of gender essentialists, which
means it can&rsquo;t actually be a demon.

The reality is that the transsexual makes a pact with demonic forces and
willingly gives the entirety of their existence over to the other side. The
person who exists prior to transition is not anyone at all, not even a fiction,
but a smooth space of pure potential awaiting its demon. In transitioning, the
transsexual adopts new modes of presentation, a new name, wields various
instruments, and speaks in incantations which conjure reality up from fictions,
much like the magician wears a costume, adopts a secret magickal name, wields
wands and daggers, and recites magickal words. Yet unlike traditional black
magick, in which a demon is summoned and overpowered to serve the will of the
magician, trans demonology is an act of retroactively reprogramming oneself. The
secret name of the magician, her ritualistic clothing and instruments, her
secret language, these all fold in on themselves in the boundary between herself
and the demon. There is no clean point of rupture between that which existed
before and after transitioning just as there is no linear continuity from the
person a transsexual &ldquo;always was&rdquo; before transitioning. The secret name of the
magician becomes the true name of the demon.

Control societies know that transsexuality must be treated as a disease, and
transitioning as a treatment (with the implicitly stated desire to cure
transsexualism if such a thing ever becomes possible); allowing someone to
transition out of an affirmative desire destablizes the entire fiction of
boundaries it so desperately clings to. Secretly, the Church knows that
repression only creates more vulnerable holes for sin to invade into. They ask
where it ends when the image of Man and Woman, the two mythical poles of human
morphology, are defaced. From the pulpit they conjure apocalyptic visions of an
age of sin where every slime and stink, every crawling ugliness, is set free.
Everything that was once man or woman becomes a liminal zone of different
signifiers churning forth: Horns, tails, wings, scales, breasts, phalli, vulvae,
unnatural configurations of limbs and organs that become impossible to
differentiate from each other in an orgy of violence and fornication. All
fictions of essential forms and functions of bodies shuffled up, making it
impossible to draw out the divisions and borders Apollonian rationality requires
to exert control.

Could this be the future S wants? Surely not, insist Church officials. Surely
she accepts the Trinity and that Jesus Christ is her Savior. She could repent
now and accept the immortality of her soul before being burned at the stake. Is
she prepared to accept the consequences of throwing away salvation, being damned
to an eternity of suffering? Does she truly want to see a world where such
monstrosities are possible?

Something else replies in her voice: &ldquo;Everything you can and can&rsquo;t imagine is
already real. You haven&rsquo;t seen anything yet.&rdquo;


<a id="orgbfef5cb"></a>

# Bibliography

Wittig, Monique. *The Straight Mind*.

Deleuze and Guattari. *Kafka: Towards a Minor Literature*.

k-punk, [&ldquo;Spinoza, k-Punk, neuropunk&rdquo;](http://k-punk.abstractdynamics.org/archives/003875.html)

Carrie Arnold, [&ldquo;The Non-Human Living Inside You&rdquo;](https://www.cshl.edu/the-non-human-living-inside-of-you/)

