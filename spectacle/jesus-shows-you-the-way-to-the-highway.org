#+TITLE: Jesus Shows You the Way to the Highway
#+EXPORT_FILE_NAME: jesus-shows-you-the-way-to-the-highway
#+DATE: [2022-10-19 Wed 17:44]

Oh man, what a fucking weird movie. This one is really hard to classify; while
watching it, the best way I could think of to describe it is if David Lynch had
grown up in the 80s and been influenced a lot by old Atari and NES games and
read a lot of Phillip K. Dick and decided to make a movie that was meant to pay
homage to that on a low budget in the form of a spy-fi comedy fever dream. A lot
of people who like film and like surrealism would probably be quick to compare
anything weird like this to David Lynch, since he's kind of seen as a gold
standard for a director who makes really surreal films. In this case I think
it's actually extremely apt though and that there are clear David Lynch
influences, from one of the major characters (the main character in this case)
being a slightly deformed dwarf to the way the character's lines are recorded to
the use of intentionally bad special effects.

For me though, it all worked extremely well to give the movie a dreamlike
quality. When making something that's meant to be surreal, especially something
in a visual medium, I think it's easy to try to go all-in on having weird
imagery that doesn't quite make sense (I know this from early attempts in some
writing and visual stuff I've made that is fucking terrible and will never be
shown to anyone). But the way that actual dreams tend to feel is that while a
lot of weird shit happens, it also has its own logic where everything kind of
makes sense but not if you think about it too much. You're supposed to be lead
along by the logic and rhythm of the story/dream, and I think this movie does
that very well. A lesser movie trying to film something that takes place in a
world that isn't the real world, a dream or a virtual world, wouldn't think to
do the things this movie does to give it that quality. I'm thinking of something
like Sucker Punch or Ready Player One as examples of movies that try to do
something kind of similar but are also stupid and have big budgets. Movies like
that don't use the medium of film itself to tell that kind of story and create
that kind of impression in the audience of it taking place in a world where the
rules of reality are different. They instead just throw in lots of big spectacle
imagery thanks to their big budgets that is meant to be weird or fantastical but
doesn't do all the subtle subliminal tricks of filmmaking to make the audience
suspend their disbelief.

Briefly, the synopsis of the movie is that there are two spies working for the
CIA in the 2040s who have to go into a VR world that is I think supposed to be
kind of similar to the "metaverse" (but not intentionally since this movie came
out slightly before that was coined) and have to defeat a virus called "Soviet
Union". Various twists and turns ensue. The movie's main antagonist, the virus
Soviet Union, is an actor in a comical Josef Stalin costume who wields a hammer
and sickle as weapons, and it also features a "country" in the VR world called
Beta Ethiopia whose president is an African man in a 1960s Batman costume and
has an Italian right-hand man named Mr. Sophistication. One of the main
characters, the derformed dwarf, dreams of opening a pizzeria and has a passion
for making pizza and has a much taller muscular wife who is a security guard at
a nightclub and who dreams of opening a kickboxing school. Early on into the
movie, this dwarf character (Agent Gagano) is captured by Soviet Union and is
dying on the table in the CIA lab where the two agents are wearing VR headsets,
and the plot becomes mainly about trying to save him while stopping Soviet Union
before it can spread and take over the CIA network. That's the best I can make
of the overall synopsis at least; in addition to it being really weird and hard
to follow at times I was also half-asleep while watching it (not because I was
bored, though).

While this all sounds like it could be used to make an intentionally cheesy
low-budget homage to spy movies, the way it's handled elevates it to feeling
exactly as if it were the dream of someone whose unconscious is littered with
pop culture references to retro video games, spy movies, kung fu movies, 1960s
Batman, Phillip K. Dick novels, and probably some other stuff I'm missing. While
I've never seen a single spy movie or the 1960s Batman show or read any of PKD's
novels besides The Man in the High Castle and Do Androids Dream of Electric
Sheep (meaning I haven't read the weirder and more spiritual PKD books this
movie is explicitly influenced by), it was still a really enjoyable ride where
most of the time I was just appreciating how well the movie manages to capture
that very specific surreal tone of an unconscious mind filled with junk media
and sci-fi. While I'm a big fan of David Lynch and a lot of other surrealist
media, usually the surrealism feels a bit more elevated and psychologically
suggestive (though David Lynch also as I said tends to do things like use
intentionally bad special effects or quirky comedy), whereas this felt more like
a lot of the dreams I've had. A lot of vaguely familiar elements from my life or
media, but all of it basically feels like absurd pointless nonsense.

The director uses stop-motion photography through a lot of the VR world scenes,
for example, to create a very odd stuttering effect that is meant to create the
impression of them being in a VR world that has this retrofuturistic quality of
having some references to old video games (particularly the opening graphics on
the CIA monitors and the chiptune score) while still being an immersive VR
world. The characters also wear paper masks with animated mouths to further add
to the impression of it being a low-tech 8-bit VR world. Again, in a dumber
movie, something like Ready Player One or Sucker Punch, I could see how the
director could have tried to make the movie feel like a video game by using
superficial imagery from video games rather than making use of the medium of
film, but the director was smart enough to not do that and instead add little
touches like the stop-motion photography and just enough details like the score
and some opening visuals in the "real" world to create that impression. Another
thing that really stands out about the movie is that the whole audio track is
dubbed over, but is also seemingly made to be an intentionally shitty sounding
dub with terrible voice acting and really weird stilted dialogue. I don't know
if that was because this is the director's first English movie or if that was
all actually intentional, but either way it works really well to further give it
a surreal dreamlike quality where everything is just slightly off and none of
the characters feel like real people. It also gives the movie the feel of almost
being like a shitty Russian bootleg or knockoff of an American spy movie that
doesn't exist.

One last thing I need to point out that was really minor but that I really
liked: There's a scene in the movie where one of the other two main agents in
the movie has his car parked outside of a hotel. I noticed that when he goes out
to his car, it's parked right in the middle of the road between the two lanes,
and the shot of it is from above as if to make sure the viewer can see this.
It's an extremely small detail that isn't drawn attention to a whole lot, but I
really liked this because it's an example of all the smart touches in this movie
to make it feel dreamlike. In the real world, someone wouldn't park their car
like that, but in a dream where every "scene" is just meant to be its own
isolated event that leads into the next without there necessarily being any
connection, something like a person parking their car in the middle of the road
would happen and you wouldn't think about it.

Anyways, I've talked about this one enough. It's extremely weird and unique, so
I give it a strong recommendation.

