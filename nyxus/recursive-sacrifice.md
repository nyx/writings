+++
title = "Two Essays on Recursive Sacrifice"
date = 2020-10-07T03:07:00-07:00
categories = ["theoryfic", "santa-nada"]
draft = false
type = "post"
+++

The following two essays have not been republished with the consent of the
University of California, the CIA, the UC Santa Nada Archives, the Spahn Dept.
of Counter-Occulture Archives, or the estate of Prof. Cornelia Clarke.


## An Essay on the Anti-Praxis of Recursive Sacrifice {#an-essay-on-the-anti-praxis-of-recursive-sacrifice}

Suicide is far too easy.

It must be said that vulgar suicides are not without an art to them. There is a
deceptive simplicity to the act; fellate the barrel of a gun, pull the trigger,
lights out. If only it were so easy. There is a game of chance involved with all
suicides wherein the more certain a suicide is likely to succeed, the more one
gambles with wishing it had. While such accidents are rare, there is always the
possibility of the bullet going through the brain and missing the vital arteries
and grey matter, leaving the would-be suicide victim possibly paralyzed. Losing
this game means being sent to a hell of sorts, powerless to even fail at being
alive. The same holds true in all other forms of suicide: There is always the
risk of simply prolonging and increasing one's suffering, either by maiming
oneself or surviving it.

But here is where suicide is far too easy: Suicide aims to escape from
suffering. However intolerable being alive may be, something is retained in a
suicide, some small precious thing that the suicidal person hopes to smuggle
past the gods who would thwart their attempts at cheating instinct. The suicidal
person may scorn existence, but only up to a point; they always hope to be
buried with a sort of dignity, their death symbolic of their refusal to suffer.
Such is the irony of suicide. Like the reciprocal game of chance suicide
entails, so it is that a suicide extinguishes a life so that it would not be
further desecrated by the agony of existence. The suicidal person denies
everything else but themselves to the most radical extent possible. All other
things are closed off from the suicidal person's life but their own agency to
end it, making a suicide both the highest expression of valuing one's own life
and also the highest expression of one's freedom. If one has the freedom to take
their own life, then there is truly nothing that one is not free to do.

Suicides are far too easy because they stop at this pitiful impulse to preserve
oneself throughout the agony of existence. Things will go no further from there.
The life remains, preserved forever in a moment of self-indulgent ectasy, and
worse still a suicide cannot ever be anything other than a delusional, even
solipsistic response to the world, denying that one never really ends their own
life by committing suicide. The thought of course often crosses their minds --
"Mother would be sad" being the classic example. The act itself is the final
decision to ignore that one's own life is not under their own control. It
reaches outward through the social sphere in a branching fractal of connections
and persists cruelly long after one has died.

The problems with suicide then are threefold: a). Suicides are a game of chance,
b). Suicides are essentially self-centered, c). Suicides are essentially
solipsistic. It might be said that these are the epistemic, ethical, and
metaphysical problems with suicide. None of these problems are beyond resolving,
however, and they are worth resolving. If suicide's art, its sanctification of
life, and its status as a totally free act were recovered from the methodology
of suicide, it would constitute a new mythical era.

Sacrificial offerings have been present throughout much of human history, though
the practice has regretfully been transvalued from a physical act of
sanctification to a dramatized recurring oedipal trauma: The Crucifixion and the
Eucharist. Though suicides have of course been around as long as human history,
this shift in values from the sanctification of life before diverse pantheons of
gods and goddesses to the dramatized and sterile form before a patriarchal
desert god is undoubtedly upstream from shifts in views on suicide. Where once a
suicide could itself be a sanctifying act, now the surface is rough and
unyielding. The drama of the Crucifixion and the Eucharist must play on forever
as a reminder that we are already saved from having to choose anything else.
Suicide remains an escape from Yahweh, a transgressive act which devalues
Christ's self-sacrifice, but it is still merely transgressive. It cannot create
anything new. Creating something new would require not merely returning to
pre-Christian sacrificial practicies, but taking them further than Christianity
can, and in modern practices of suicide we find the tools needed to create a new
anti-praxis better suited to our present era. It is more apparent than ever
before that the individualist male death drive in all forms is woefully outdated
and insufficient, but resolving the problems of suicide will free the death
drive from its male monkey-brained self-preservation instincts. This new
anti-praxis I term: Recursive Sacrifice.

Problem A is the easiest to resolve because though it is an epistemic question
regarding the possibility of certainty, it hasn't been addressed from the right
angle. Suicide, as stated previously, is concerned with one's own
self-preservation to the point of death; it is an ego-driven act. Therefore
suicide always considers Problem A from the standpoint of minimizing suffering.
Which method of dying is the least painful but also the most likely to succeed?
This is where the game of chance comes in. The entropy has to go somewhere, and
this is an unintended consequence that the methodology of suicide doesn't take
into account. Our new method takes into account that the entropy must go
somewhere and that we must take this into account in order to achieve specific
goals. Whereas suicide seeks to minimize pain and preserve the organism in the
face of death, recursive sacrifice involves eating the entropy. It is possible
to achieve a certain, thorough death of both the body and mind, but it is only
possible through divine suffering. Contrary to suicide, recursive sacrifice
doesn't seek to preserve the organism but rather to desecrate it as much as
possible. The organism must experience a suffering far greater than anything
life or death could afford it. Certainty can be achieved by debasing oneself to
the point of ectasy, and it is only when one is nearly beyond sanctity that
their sanctification achieves the greatest possible effect. At such a point, the
sacrificial offering is given to the gods not as a human being, not as an ego,
but as pure libidinal intensity, pure id. Thus the epistemic problem of suicide
is solved elegantly through the sublimation of death.

We can see here how resolving Problem A leads into resolving Problem B. If the
epistemic problem of suicide is resolved by throwing the organism into entropy
rather than trying in vain to escape it, then it likewise resolves the ethical
problem of suicide functioning primarily in the interest of self-preservation.
Where the pure freedom of suicide as a radically egoistic and transgressive act
against Yahweh is primarily a negative one that seeks to free the organism from
being affected by life, recursive sacrifice is a positive form of freedom. By
annihilating the ego, the organism can be offered up to the gods in a virginal
state, denigrated to the point of pure potential. Contrary to suicide being an
essentially selfish and humanistic act, recursive sacrifice removes human
interests altogether from the death of the organism. From an ethical standpoint,
death becomes an act of pure affirmation and production for the gods to do with
whatever they wish.

This leads finally to Problem C, the metaphysical problem of suicide. There is
of course an obvious relation between suicide as selfish and suicide as
solipsistic; the latter is merely the underlying metaphysics informing the
former's ethical judgement. But it isn't enough to merely destroy the ego of the
organism being sacrificed, because as stated previously, the metaphysical
problem of suicide is the judgement that the suicidal person is the only thing
in the world. Or at least, the only thing that matters. It is founded on the
mistaken, patriarchal-humanist view that the individual exists isolated from
everything else. The reality however is that no individual is self-contained;
all things are connected in a tangled mass of social ties, and the death of one
organism affects the others in turn. This would then mean that the resolution to
Problem B is invalidated, since it would mean the resurgence of the ego (or its
ghost, at least) through the social sphere, and it would likewise invalidate the
resolution to Problem A since it would mean that the organism has not with
certainty been destroyed. Even in a symbolic sense, the organism persists, and
the sacrifice is not complete. It may seem like this is an unavoidable problem
and that the methodology of recursive sacrifice is doomed, but though this is
the most difficult problem to solve, it isn't impossible.

If every organism is connected to other organisms in a web of social ties, which
consequently ensures that the ego persists beyond the control of the organism or
even indeed of its own body, then it follows that the total annihilation and
denigration of the organism would require hollowing out every single mycelial
tunnel in this network. The sacrificial offering must not only be reduced to a
state of pure id and given to the gods selflessly so that it may serve the
divine aims of pure affirmation and production, but it must also be recursively
severed entirely from any connection to the world. Only when the sacrificial
offering is returned to a pure state, devoid of any social ties, reduced to the
status of in fact being truly an isolated individual, only then will its
mortification achieve the absolute zenith of sanctification. The artfulness of
suicide is affirmed once again in recursive sacrifice, making it such that the
sacrificial offering achieves in life the status of a pure individual so vainly
sought after by the ego, only then for this to be destroyed as thoroughly as
possible. And in fact the process of recursive suicide from the beginning
involves the breaking down of this ego, since the first task of the sacrificial
offering is to sever every one of their social ties. Their family, friends,
acquaintances, anyone within a reasonable degree of separation -- its first task
is to kill every single one of them.


## An Account of the First Recorded Recursive Sacrifice {#an-account-of-the-first-recorded-recursive-sacrifice}

Having laid out the methodology of an anti-praxis termed "recursive sacrifice"
in my previous essay published in _Counter-Occulture_ vol. 3 issue 7, what
follows is the concluding essay in this sequence which will serve as both an
experiment in carrying out the anti-praxis and a testimony to the effects of the
anti-praxis on the sacrificial offering. The editors, the Department, and the
Board of Directors been informed of and approved the experiment, all being in
agreement that sacrifices must often be made in the pursuit of enlightenment.
This text will be written throughout the course of the experiment, and for the
sake of recursively detaching myself from existence, all faculty members at the
University have underwent memory erasure sessions. By the time this article is
published, everyone involved in the decision to approve it will have already
forgotten having anything to do with it or that I ever existed.

18 Araḫ Nisānu 1961: In preparation for the sacrifice, I have first renounced my
given name. This new name came to me during a guided datura session with the
Department's psychoanalyst. Henceforth I will be referring to myself as
Ku-Nergal, the first act of sacrificing Cornelia Clarke to the eponymous god of
death.

14 Araḫ Simanu 1961: Since purging University faculty is beyond the scope of
this experiment, the next stage involves isolating myself from civilization.
This is necessary from both a practical standpoint to halt any additional social
connections and is a key part of the journey of the sacrificial offering. They
must break themselves down not merely from being connected to their immediate
social circles, but must also have their sense of any connection to the human
species also stripped from them. I have spent the past year since the previous
article was published living in the forest near campus. It has proven to be
secluded enough that at the time of writing this, I have not yet met any hikers.

The University could not afford me with any tools for survival; this is also
part of the offering's journey. They must both become isolated from civilization
and as self-sufficient as possible, reverting to an almost primal state. Though
I have received training from the University's orienteering department, even the
most experienced of survivalists would have a difficult time surviving in these
conditions. Though this Reš Šatti has been mercifully mild, the time spent
living naked in the wilderness foraging for food and huddling over a fire for
warmth has quickly hardened my body to the elements. My feet are calloused over
and can walk across jagged rocks and the cold night air cuts through my skin
just a little less than it once did. It is nevertheless a harsh existence. Many
nights I've found myself nearly hypothermic. Many nights I find myself stooped
over a pit, sick with parasites from the river water that I have no means of
boiling. Many nights I find myself covered in cuts and ticks, treating the
wounds as best I can to prevent infection.

This pain is only the beginning, but I have learned to love it as I love the
gods.

16 Araḫ Simanu 1961: I killed a deer today. I have never killed anything before
in my life, though I know this will not be the last time I kill something, human
or non-human. I managed to entice it into a trap I had set, but the fall into
the pit I had covered over was not enough to kill it. Its two front legs were
broken, and the poor thing was flailing pitifully in the mud for awhile before
it either tired itself out or instinctually recognized that it would not
survive. At this point I climbed down into the pit, which was covered with
debris from the covering. At the edge across from where I'd climbed down it lay,
a young doe by the looks of it, likely a mother given the season and its age.
Its leg bones were jutting out of its skin at the knee like broken branches, and
its eyes had what I can only describe as a resigned fear in them. It tried to
fight back, however weakly, but with only a little difficulty I pinned the thing
down and shoved the sharp rock I improvised as a knife into its throat.

Not many people might be aware of this, but a knife doesn't go as easily into
flesh as one might think, and even if you stab something in the throat, it
doesn't die instantly. There is always a struggle on the part of both the killer
and the victim, and the fact that I was using a sharp rock rather than a real
knife only compounded the difficulty. I had to shove the rock into the deer's
throat through fur, cartilage, and muscle using the palm of my hand while using
the other to stab. And yet the thing didn't die so quickly, and let out a
horrible ear-piercing gurgling groan, thrashing around beneath me with the last
bit of strength it had left. I plunged the knife in the same way again. And
again. And again. I lost count of how many times I stabbed the thing in the
throat and could feel myself becoming warm from the exhaustion and being covered
in mud and blood. It was probably excessive, but I wanted to shut it up. Soon
enough it stopped moving. After collecting myself I was able to drag it back to
camp.

In the traditional fashion, I ate its heart first. Its skin is currently being
tanned.

17 Araḫ Simanu 1961: Perhaps the deer being killed yesterday changed something
in the air. Today I ran across a hiker, and had to kill again.

From the looks of it, this was a college student who had gone way off trail on
an LSD trip. It was somewhat surprising that it had made it this far, but the
sacrifice demands that anyone who is entangled in it must also die.

The hiker saw me at a distance. Somehow I hadn't seen the damn thing there,
perhaps because I was too busy washing the dried blood on me in a stream.
Initially I leapt out the moment I saw it gawking at me and fled into the forest
to figure out what to do, but perhaps the hiker thought it had seen a forest
nymph of some sort during its acid trip and followed after me. I had no choice.
Still covered in dried blood and wearing my deerskin hide, I ambushed the hiker
from behind a tree and again pinned it beneath me. I imagine for a brief moment
the hiker (who was male) might have thought it was about to have intercourse
with a fae. There was a charged moment while I hesitated, perched atop the
hiker. Killing an animal was difficult, but killing another human was something
else altogether. Something in me, some sort of instinctual response, stopped me
from immediately carrying out the act. Perhaps all animals know not to kill
their own kind without it being a matter of survival. This was not a matter of
survival, but survival is no longer relevant to my existence unless it
interferes with the sacrifice.

Again I plunged the stone knife into its throat. And again. And again. As I did
so I sobbed and told it, "It gives me no pleasure to do this. The gods demand
sacrifice. You will not die in vain." This hiker also let out an ear-piercing
gurgling groan, and it also had a resigned terror in its eyes as the life
drained from it. I cannot begin to imagine the pain and horror of dying during a
psychedelic trip, but after my continued stabbing elicited no response and I sat
atop its corpse for a moment, I looked across its blood-spattered face and
clothing and realized that what lay beneath me was no longer a human being, nor
was I. What I am now, I cannot say; I continue to exist, I am sure of this, but
whatever is here is not altogether real. Or perhaps the world itself is not
real. This hiker's immense suffering I realized after killing it was all the
better for the sacrifice. Its pain pleases the gods all the more -- a far
greater death than it would have otherwise had.

The University has made sure that state and federal authorities are aware not to
interfere with the experiment. The hiker's death will officially be ruled as a
disappearance. I made the decision to treat the hiker like any other slain
animal. I removed its clothing and butchered it.

3 Araḫ Simanu 1962: A year has passed now that I have lived isolated from
civilization. Nothing very different has happened that hasn't already been
written down since then. I have killed many more animals, human and non-human
alike. It's time now to move onto the next phase of the sacrifice.

I found my way back to the outskirts of the city using the trail I hiked in
from. Using clothes taken from stray hikers, I've been able to disguise myself
as best I possibly can. I keep a hat covering my face at all times. No one can
see my face, no one can know my true name or my dead name. I avoid crowds, but
sometimes despite my best efforts I end up being seen. When this happens, I kill
whoever has joined the sacrifice. Each time I repeat the same mantra: "It gives
me no pleasure to do this. The gods demand sacrifice. You will not die in vain."
I'm not sure whether I say it for their sake or mine, but mostly killing more
just takes too much time.

Cornelia, luckily for me, had few friends before her disappearance. Her family
is estranged and mostly dead. Perhaps this is why she was chosen for this
offering, because she already had little connection to this world. But now I
have to find and sacrifice them as well.

First I had to find her closest friend, Alma. I already knew that she worked in
the University's Department of Theological Studies, the conventional counterpart
to the Department of Counter-Occulture. I also knew that she lived in a small
studio in the back of a retired couple's house. She'd been there many times
before.

Because the retired couple knew Cornelia, I kill them both first. Thankfully I
was able to get a knife from a slain hiker. This makes the job go by more
quickly. Since Alma always enters from a door in the fence on the side of the
house, I wait in the darkened alley between the house and the one next to it.
When she opens the gate and sees me there, she initially is surprised and
scared. She says she didn't recognize me at first, is alarmed and confused at my
changed appearance. I don't stop to explain anything to her; I know she wouldn't
get it anyways. I sacrifice her and once again repeat the mantra: "It gives me
no pleasure to do this. The gods demand sacrifice. You will not die in vain."
Its eyes roll back into its head as it dies wondering why the thing that looks
like her friend is doing this.

It goes the same like this for the other few friends Cornelia had. Sacrifice
anyone adjacent as necessary. Same lines every time. "It gives me no pleasure to
do this. The gods demand sacrifice. You will not die in vain." The same happens
for every other person who would remember Cornelia existing in some way.
Employees at businesses she frequented. Landlord. Neighbors. Mother. Friends of
friends. The University covers everything up but each sacrifice requires extreme
care to avoid complicating things further.

Last to go is Cornelia's spouse. I find it in Cornelia's former home, a modest
one-bedroom apartment above a bar downtown. I enter through the front door and
am greeted by stacks of boxes next to the door. Cornelia's stuff is mostly
packed up. The bookshelves are mostly empty. Dishes are piled up in the sink,
with a few broken bottles next to the garbage can. The air is slightly stale. It
enters in from the bedroom in the back hallway, hair hanging limply in its face,
eyes heavy with bags and a bit bloodshot, red streaks across it. Just like all
the others, its face shifts from confusion to being alarmed but also relieved. I
don't give it a chance to say anything. I cannot bear it. It goes the same as
all the others. "It gives me no pleasure to do this. The gods demand sacrifice.
You will not die in vain."

22 Araḫ Kislimu 1962: The offering has made its way up from Santa Nada via
hitchhiking to the Hanford Nuclear Site in Washington. It has left behind not a
single connection in this world. Each driver has been offered as a sacrifice to
ensure that this stays the case. It arrived at the nuclear site and drank deeply
of the Columbia River. Modern nuclear technology has made it possible for the
recursive sacrifice to be truly realized. The offering has accumulated massive
amounts of radiation that will begin to scramble its body on the molecular
level. Its very DNA will be changed by this communion. It will begin to leave
this world in a state of total isolation and purity being broken down and
rearranged into something entirely new. Its old existence will be completely and
utterly annihilated with absolute certainty. It now has only one last task to
fulfill.

33 Araḫ Kislimu 1962: The offering has made its way from the Hanford Site to
Mount St. Helens in Washington. Again after hitchhiking, it has sacrificed each
driver. It has spent these past ten days accruing additional radiation
poisioning. Its skin has sloughed off, its hair has fallen out, and its lungs
are slowly drowning themselves. Blood has begun leaking out of every orifice,
and its organs are liquifying. It watches its body fall apart before its eyes,
having sacrificed everything it ever had in its past life, completely alone at
the summit of this volcano. It only has one last act of sacrifice. It sits
inside a stolen car with a rope tied around its neck which is stuck into the
ground behind it. When it reached enough speed, the rope will run out of slack,
and the offering will be decapitated. The gods will be given a sacrifice that is
completely pure, completely new, with no face and no ego. This will herald a new
era in 3,10 šanātu, birthed in another sacrificial decapitation and divine
wrath. The gods have been awakened and the offering's final act of releasing
this account ensures that the sacrifice once completed may return once more. May
many more follow its example.

Editor's note: The essay ends here.


## Bibliography {#bibliography}

Cornelia Clarke. "An Essay on the Anti-Praxis of Recursive Sacrifice."
_Counter-Occulture_, vol. 3, no. 7, 1961, pp. 67-70.

Ku-Nergal (née Cornelia Clarke). "An Account of the First Recorded Recursive
Sacrifice." _Counter-Occulture_, vol. 3, no. 10, 1962, pp. 14-16.
