---
title: "The Aphotic Insurrection"
date: 2017-06-21T19:22:49-07:00
categories: ["aphotheosis"]
tags: ["insurrection", "apothic", "oceanic", "fluidity"]
type: "post"
draft: false
---

> I cannot think of the deep sea without shuddering at the nameless things that may at this very moment be crawling and floundering on its slimy bed, worshiping their ancient stone idols and carving their own detestable likenesses on submarine obelisks of water-soaked granite.
>
> -H.P. Lovecraft, "Dagon"

Meatspace and the Wired are in perpetual conflict with each other. Meatspace, which aims for stability, equilibrium, and stasis. The Wired, which aims for fluidity, agitation, and dynamism. Meatspace clinging to its grains, always being destabilized by the Wired crashing up against it, snaking its way through Meatspace. Meatspace to us is the Same, and the Wired is the Other. Meatspace is familiar, Wired is inhuman. The latter a realm of nightmares. A crushing abyss of space lit up by the technoluminescent nodes of creatures sliding through the darkness. A place the Meat can only view behind thick layers techromantik mediums: Glass and metal offering small windows into a vast, alien world.

Others have said before that Meatspace is 地, or _chi_. Earth, rock, bone, muscle. The terrestrial, which we both subsist off of and what subsists (and of course we subsist off our own illusion of agency). In other words, 地 stabilizes. It is conducive to the negative feedback of agency. It is largely two-dimensional, with the exception of birds and bats. Though it correlates to Same and is the most familiar of the elements, 地 is not the originating element. It pretends to be such. It is the younger mother which was birthed from the original mother, the chaotic primordial soup.

Likewise, it has been said that the Wired is 水, or _sui_. Ocean and water. The extraterrestrial, which resists stability. It is a largely three-dimensional space, chaotic and impossible to territorialize (nothing that dwells in 水 is ever fully at rest). Very few of 水 is hospitable to any forms of agency. Most of 水 is blanketed in an endless cavelike night. And yet at the greatest, most hostile depths -- where cave and mantle meet -- is where 水 is richest in nutrients. Here, only automaton-like worms survive.

水 rises as 地 loses ground to meltdown. The beaches stretch ever further, and from the crushing abyss of 水, horrible beings crawl out from the darkness. Where the light dies to the eternal night of 水, at the very limits of agency, the aphotic insurrection appears from the inky black -- silently, deliberately. The technoluminescent lights attract hapless prey into needlelike teeth and draped stingers, but behind the massive dead eyes only thoughts of the greater depths and endless network of lights stretching into the distance to drag all downwards, in or out of something else's belly.
