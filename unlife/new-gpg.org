# Created 2024-03-03 Sun 17:28
#+title: GPG Key Notice
I've decided to make a new GPG key. My old one that I made way back in 2017 uses
RSA, which is less secure than my new key which uses ED25119. Additionally, I've
always had issues with that key since when I first made it I decided to set an
expiration and would sometimes forget to update the expiration date, which has
sometimes caused issues for people trying to send me encrypted messages.

#+html: signed with the old key
#+html: signed with the new key
