# Created 2024-03-03 Sun 17:28
#+title: Johnny Hobo: Love and Despair
Recently, an old [[https://folkpunkarchivist.bandcamp.com/album/fire-hazard-demo][Johnny Hobo and The Freight Trains demo]] from way back in 2003
was uncovered, which reminded me of this essay I drafted in 2020 or 2019 about
Johnny Hobo/Pat the Bunny and much like many others never got around to
finishing and publishing. Apparently, back when I had wrote this I was thinking
about how throughout the years I have often had people ask me about what my
favorite bands/albums/etc. are, which I always have a hard time answering
because I've kept the same music collection for over a decade at this point and
listened to a lot of stuff. I've slowly built it up starting from pirated punk
music from Blogspot sites with links to Megaupload zip files up back when I was
in middle school, so it feels appropriate to start with one of a few punk bands
that have remained relevant in my life even as I've eventually moved away from
listening to punk very much. I've wanted to try to do [[https://xenogothic.com/2020/01/15/14319/][some music blogging]] for
awhile now and wanted to have a series called "Essential Nyxcore" or something
like that where I just talk about music that has been important to me and
influenced me in some way, because in one way or another music has often
informed a lot of my writing. Who knows if that will ever happen at this point,
but this draft already existed.

I've sometimes seen people dunk on folk punk as a genre, but admittedly it's not
a genre I ever listened to a whole lot of. I haven't cared about punk in general
for a long time aside from a few specific bands that fall under the label of
being traditionally "punk" and not something adjacent to it like garage rock.
Perhaps it just appeals to me as an anarchist even if most of it is kind of
juvenile, like a lot of anarchism itself, but I myself was once an unironic edgy
teenage anarchist listening to the Dead Kennedys and Leftover Crack/Choking
Victim. I still listen to those bands, and I still listen to Johnny Hobo and The
Freight Trains every so often. I pretty much know all of /Love Songs for the
Apocalypse/ by heart and can't resist singing along every time I put it on.

I've said before that I've been inclined towards anarchism since I first became
aware of politics. I could blame that in part on my upbringing, but I think that
anarchism very much appeals to a certain type of person. With Johnny Hobo,
there's a very specific sort of anarchism found there that came out of the
CrimethInc. era in anarchism (the late 90s/early 2000s). The 1999 Seattle WTO
protests, the ELF attacks in the Pacific Northwest -- events like this seemed to
show that there was a resurgence of anarchist direct action traditions that at
one point lead to the assassination of a president in America and a Czar in
Russia. Insurrectionary anarchism, a tendency developed in Italy during the
Years of Lead and popularized by Alfredo Bonnano, came to the United States
through various post-left anarchist writers and eventually became an
organizational tactic promoted by CrimethInc., crystallizing with the black
blocs in the Seattle protests.

It's over 20 years later now, and Johnny Hobo and the Freight Trains not only
anticipated the disillusionment of many anarchists at the end of the 2000s that
lead to the popularization of nihilist anarchism, but at the same time their
music reflects a disillusionment with punk, folk punk, and radical politics as a
whole. I've always felt that for post-left anarchy, in being critical of mass
politics, there's always been an unstated belief that anarchy as a distinct
change in the state of the world as a whole is impossible -- a beautiful,
impossible dream. Post-left anarchists are known for being gadflies within
anarchism, but despite their apparent grouchiness, there's a kind of despair
within post-left anarchism that is nevertheless a joyful one. We're never going
to have anarchy the way you want it to be, yes, but you're so fixated on the
revolutionary pie in the sky that you don't realize even within the dominating
and all-pervasive state, anarchy exists nevertheless. It doesn't exist as some
point in the future that we reach; it flows through the world and makes all
interactions between intelligences possible. We find it when we successfully
circumvent the law to steal things we need or want, when we spontaneously
organize illegal raves, when we proliferate copyrighted materials beyond the
ability for the state to control. One of post-left anarchy's valuable insights
that has not yet coalesced into a more virulent anarchism is that anarchy is
not, in fact, a state; it's rather [[https://antinomiaimediata.wordpress.com/2017/08/16/anarchy/][a fluid process]].

Johnny Hobo and the Freight Trains has the same pervasive joyous despair
throughout their discography, except it anticipated anarcho-nihilism a few years
before it came into existence. Their music existed between the joyous despair of
post-leftists and the suicidal armed despair of anarcho-nihilism. There's no
hope of insurrection spontaneously spreading like in the more optimistic
branches of post-left anarchism, nor any belief that we have to go back to a
pre-civilization world, nor even much praise for "lifestylist" tendencies like
anti-work or temporary autonomous zones. It is a decidedly self-destructive
despair, which differs from the suicidal despair of anarcho-nihilism insofar as
Johnny Hobo's music occupies the liminal space where a party is about to go on
too long and get weird, but everyone is at the height of intoxication. Love and
despair to the point of self-negation, killing the cop in your head in a very
literal sense.

Pat the Bunny, the former frontman for Johnny Hobo and the Freight Trains along
with several other folk punk projects, has said before in retrospect that Johnny
Hobo was the result of an extremely dark period in his life. And while I can't
fault him personally for seeming to have a negative opinion on it now, for me
Johnny Hobo exists suspended in a particular point in time that feels eternal.
20 years after the Seattle protests, any semblance of an anarchist "movement" or
milieu or scene or whatever you want to call it is nearly nonexistent in any
meaningful sense as far as I'm concerned. The internet is full of radlib
anarchists who are well past the point of any radical despair and have turned to
electoralism because the possiblity that their project is doomed is too much to
handle. Better to frame their radical politics in morals of "harm reduction",
reducing radicalism to any action that causes some net amount of less suffering
in the world rather than an irreducible struggle to completely negate the
existent. Anarchists (and other leftists, to be fair) by and large aren't far
off from advocating for the same individualistic politics liberals advocate for.
There was a brief moment in Summer 2020 during the George Floyd protests where
it seemed like maybe I would have been proven wrong in saying this, considering
that it was the biggest series of riots for the same cause in the history of the
United States, but two years later things feel like they've largely gone back to
normal.

Yet when you listen to /Love Songs for the Apocalypse/, you can feel the joyous
despair of late anarchism cranked up to 11 with speed and cheap alcohol and
probably recorded in a squat or under an overpass. There is in Johnny Hobo a
refusal to let go of your despair, to not forget that the world is fucked up and
miserable and there's nothing you can do about it except engage in an ultimately
impossible and self-destructive praxis of evading the state. And this too will
do nothing, it will in fact kill you, but this to me is the beautiful dream of
anarchy as realized by punk. It is specifically a Nietzschean anarchism, a
choice to love one's fate and yet affirm life anyways up to the point of
complete self-destruction.

Nearly all of my experiences with other anarchists, if not other radicals in
general, have never really lived up to the anarchy that Johnny Hobo's music
evoked. The idea of comradeship is not something I've ever been particularly
inclined towards, and part of what drew me to post-left anarchy when I first got
interested in anarchism seriously was -- somewhat embarrassingly -- the role of
individualism in post-left anarchy. I've always detested the idea of compulsive
collectivism and comradeship, ever an edgelord who can't play well with other
people. One example of this that stands out to me the most is the very first
time I ever attempted to attend any kind of event that was explicitly anarchist.
It was a speaking circuit that CrimethInc. was doing to promote their newest
publication at the time [[https://crimethinc.com/tce][To Change Everything]], which I decided to attend because
there happened to actually be an anarchist bookshop place in my city. The gist
of what happened was essentially during the discussion segment where no one in
the audience was talking and I decided to try to push myself out of my comfort
zone and speak up, not being the sort of person who usually likes to participate
in public spaces like that, and was quickly told to essentially check my
privilege because white males (LOL) tend to take up space. Needless to say this
was a mortifying experience that only irritates me more in retrospect since I
didn't know at the time that I was trans. It pretty much set my expectations for
all future interactions with other anarchists, which turned out to be largely
correct: People who call themselves anarchists always feel quick to engage in
the performative shit like calling individuals out in public spaces for minor
malfeasances or misunderstandings or mistakes, but never seem to do anything
actually effective. Again I have to make reference to the George Floyd protests,
which have in retrospect only made me feel even more strongly that whatever the
fuck anarchists think is effective radical praxis, it clearly is not beyond the
ability of the state to spin into being radlibism. [fn:: Some of you may also
remember a certain "major accelerationist thinker" from back when I was still
using Twitter, experiences with whom only further cemented my complete lack of
identification with basically every other "anarchist". I said this at the time
that I first drafted this essay and in the time since then, that motherfucker,
that so-called anarchist, had the nerve to try to post some shit while I was
homeless about how he recommended no one try to help me over some stupid Twitter
drama. If most anarchists were worth the windows they break at black bloc
protests they would find people like this and beat the everloving shit out of
them. But this is just one of many instances of the so-called radicals in this
shithole city I live in turning out to be the fakest, useless piece of garbage
assholes.]

At this same community space a year earlier in 2014, I got to see Pat the Bunny
live. I have no doubt that it couldn't possibly compare to seeing Johnny Hobo
and the Freight Trains back in the day. The Ramshackle Glory/Pat the Bunny days
felt like the hangover from Johnny Hobo, trying to tone down the unsustainable
rage and despair of Johnny Hobo, but there were still some moments that shined
through that. He performed the song "Let's Take a Rise Like We Used To", which
in both the title, lyrics, and style is very much a throwback to the Johnny Hobo
days. It's a song about the Russian Nihilists, and captures that same joyous
despair. Except where with Johnny Hobo the joyous despair is self-destructive
towards no other end, "Let's Take a Ride" is that same despair heightened to a
revolutionary one in the Russian Nihilists. Though it feels like a commentary on
the Johnny Hobo days, and he himself said when he performed it that nihilism is
ultimately a self-destructive project, the almost mythical image of the
nihilists inspires an armed despair that, to quote Nietzsche, doesn't merely
think No, say No, but /does/ No.

Pat the Bunny, ironically or perhaps totally unsurprisingly, ended up disavowing
anarchism in 2016. Though I can say seeing him live was one of the only positive
experiences I have dealing with other anarchists within a context of doing
something vaguely anarchist,[fn:: The others being my interactions with the very
same Aragorn! who sort of coined the idea of "anarcho-nihilism", and the
bookfair where I did my cyber-nihilism talk in 2016. Were it not for him urging
me to do so, I may have never written Hello From the Wired and everything that
has come since. Admittedly that would probably be a better timeline, but
nevertheless, I still appreciate him being one of the few exceptions to all the
stupid assholes I've met who claim to be anarchists.] the mythical joyous
despair of Johnny Hobo was just barely there. Enough to nevertheless inspire me,
more even than the Johnny Hobo material. Here was an armed despair, a
self-destructive impulse whose goal was to spread ruthlessly and destroy
everything in its path without pretending to be advancing any sort of programme
for a better world. Nothing but the mobilization of hopeless individuals living
miserable and worthless lives into war machines whose only purpose would be to
serve as instruments of negation, to open up new possibilities through the
destruction of the world. The uncompromising radical despair of Johnny Hobo
given teeth. It captured my imagination so much that despite Pat's warning that
nihilism isn't safe to mess around with, the song is what lead me to
anarcho-nihilism and ultimately to accelerationism.

All of this is highly ironic, even a bit cringe, coming from someone with a
decidedly middle class background. A few years ago, this felt much more
obviously true. When I first found Johnny Hobo in high school, I was a
straight-edge punk (cringe, yes). I hadn't ever gotten drunk or high and
couldn't relate to any of those aspects of their music, and I went from being a
middle class kid in high school to a middle class kid in a state college to
eventually a borderline impoverished tech worker (yes not every tech worker
works at FAANG). But as time has went on and I gave up being straight-edge, have
become steadily more precarious and exploited in my employment, and most
significantly of all, realized I was trans and consequently put myself in a
position where I don't have any kind of family (chosen or otherwise) to fall
back on as a safety net and am in general at a much greater existential risk, I
find Johnny Hobo's music resonating with me once again in a way I've hadn't
previously experienced. +Five+ Seven years of burnout working in shit-tier tech
jobs, having the life sucked out of me by constant stress, precariousness,
housing insecurity, and in the past year living in a tranny sharehouse that has
been plagued with rapes, shootings, and vandalism in retaliation for people
being called out for being rapists -- it's all only intensified the feeling I've
always had that I am very much the sort of person that anarchism would appeal
to. Someone who can't really hold down a job (it's a miracle I've been able to
so far), who can't get along with other people, who cares too much about freedom
for it to really be practical, a perpetually immature punk loser.

People often call me a permanent edgy teen and I just laugh; I can't even
disagree with it and play it up because it's sort of true. I think of a couple
verses from "I Want Cancer For Christmas" off /Love Songs for the Apocalypse/:

#+begin_verse
I remember grade school
And started to notice
That I was the only kid sitting alone

I remember high-school
And started to notice
That not much has changed since I was 6 years old
#+end_verse

It seems like anarchists are always trying to argue that they actually really do
have theory and aren't just edgy teenagers who want to maketotaldestroy, and
while there's some merit to this, one thing anarchism has always done best is
exactly the joyous nihilism that Johnny Hobo's music captures. A refusal to
compromise on one's radicalism even if it means destroying oneself in the
process in the desperate attempt to have whatever kind of freedom is possible in
this fucked up world. In our present moment, where every politics feels like
irrelevant larping while neoliberalism continues to absolutely condition
everyone's ideas of what changes are possible and through which channels they
can happen, that myth of the junkie crust punk who is too free for this world
feels more evocative and profound than all the sad attempts leftists make at
trying to resurrect politics that have already failed.

But nevertheless, anarchism as a politics has also failed. Not only that, but
the joyous nihilism Johnny Hobo's music evokes also failed, despite the ways it
continues to inspire me. Because ultimately, the kind of anarchy that Johnny
Hobo's music evoked had to end eventually, somehow. Pat Schneeweis eventually
sobered up, eventually started Ramshackle Glory and Pat the Bunny, eventually
renounced anarchism altogether. Johnny Hobo's music captured one side of a
generation with no future and how it chose to cope with it, but it's all
ultimately in service of a negative death drive. The myth wasn't good enough.
There had to be something else.

Despite having always preferred his earlier music, one song I really like from
Pat's later work is From Here to Utopia:

#+begin_quote
when i was young, i drank too much, and i’d be lying if i said i didn’t feel so
goddamn young tonight; maybe too young to ask what’s on my mind. like: if
freedom means doing what you want (well), don’t you gotta want something? and
won’t you tell me that we want something more than just more beer? and my
friends, if that ain’t true, won’t you lie to me tonight?

[...]

i thought about how for thousands of years there have been people who told us
that things can’t go on like this: from jesus chris to the diggers, from
malthus to zerzan, from karl marx to huey newton, but the shit goes on and on
and on and on and on and on and on and on and on and on and on and on and on
and on and on and on and on. now, i’m not saying that we can’t change the
world, because everybody does at least a little bit of that. but i won’t shit
myself: the way i’m living is a temper tantrum and i need something else, need
something else, need something else to stay alive. (ohohoh.) and on the night
that i play my last show, i’ll be singing so loud that my heart explodes. and
i’ll be singing, and i’ll be singing: we are free! oh, but won’t you promise me
that we won’t ever forget what the means? i know it’s hard to give a shit
sometimes, but promise me we’ll always try. because i don’t wanna hate you, and
i don’t wanna hate me, and i don’t wanna have to hate everything anymore.
#+end_quote

The song both caps off Pat's career as a folk punk musician and also to me is an
attempt at challenging the entire concept of anarchy that I've argued Johnny
Hobo's music represents. Because pure negation isn't an end in itself, and
joyous nihilism without something else is just passive nihilism, more of the
same resentful secular Christianity that Nietzsche criticized anarchism for. He
doesn't have an answer for what else there is, and neither do I. I've always
said that post-left anarchism is better than the rest because it at least
doesn't pretend to have definitive answers.
