#+TITLE: Fragment: Dark Cities, Glittering Matrices
#+EXPORT_FILE_NAME: dark-city-matrix
#+DATE: [2023-03-29 Wed 20:05]

One of the first things I ever drafted for this blog was an essay in which I had
wanted to write up an analysis of the Matrix series (this was in 2021 and I had
just seen the newest Matrix movie) and the 1998 movie Dark City. Specifically, I
had wanted to focus on analyzing the strange and mystical aspects of both films,
which I've interpreted along a Fortean or Interdimensional Hypothesis line of
thinking to be essentially a restatement of various archetypes and folklore that
has existed throughout human history. The 90s were a weird time where a lot of
media seems to make references to these sorts of things, unintentionally or not,
and in fact there's a whole book that was written on this topic called
TechGnosis (though I haven't read it yet). I think The Matrix is probably one of
the most successful examples of this because it essentially channels some of the
most famous archetypes of Christianity (specifically with a Gnostic flair),
which I interpret as being why The Matrix has ended up being one of the most
influential movies on pop culture to come out in (give or take) the past two
decades.

Because I drafted this essay about /five movies/ back in 2021 and don't have the
energy or interest to rewatch the entire Matrix series again, but also have been
having a moment today of lamenting how many of my drafts that have been rotting
in my notebook for years now are basically beyond the point of being useful to
me for anything, I'm going to just release this as-is since it's better than
just never posting any of this anywhere. I'll probably end up rewatching Dark
City and writing up a proper post elaborating more on it, because it's the more
interesting movie to me anyways and I like it a lot more than The Matrix.

I'll just post this in the main blog instead of the movie blog because
everything I post in Spectacle is supposed to be as fresh as I can manage,
whereas this post is way too overdone and stale.

** Introduction

Most people have probably seen The Matrix, or at least are aware of it. I would
say that it's probably one of the most influential widely released movies of the
past 20-ish years, both in terms of its influence on other movies and its
influence on culture in general. As this century continues to drag on, it only
becomes all the more undeniable how much The Matrix has coded and overcoded the
ways that many people conceptualize the world, vis a vis redpills and bluepills
for example, or a general tendency to reduce a spectrum of different ways of
viewing the world down to a binaristic idea of understanding the Truth or not.
Despite this, no one to my knowledge has ever really tried to interrogate this
influence, which is rather strange to me. What is it about The Matrix that has
made it so influential over the ontological topology of the alt-right and
everything to come after it, a grouping of political movements that has arguably
and unfortunately been more influential than anything else in the past decade?
It's only all the more vexing when considering that the Wachowskis are known to
be two trans women, and that much has been said about the trans themes in The
Matrix.[fn:: I'm not going to touch on this in this post because it's been
talked about to death and would have to be its own entirely separate focused
discussion.] Of course, reactionaries have never been above appropriating ideas
from demographics that they despise, but even so, if there really is such a
strong trans subtext in The Matrix, then that only makes it all the more
interesting that it's fit so readily into their worldview.

Of course, the reason why this essay was instigated in the first place was
because of the release of The Matrix: Resurrections, a movie I wouldn't have
ordinarily even bothered to see (not in theaters at least) if I hadn't been
taken to see it. What was my initial thoughts on the new movie turned into me
rewatching the other three movies and thinking of the series as a whole. I'm
going to just save my thoughts on it for later because I can already imagine
what people are going to think about it and am going to preemptively defer
talking about it until the structure of my interpretations of the rest of the
series is in place.

The other subject of this essay is Dark City, a movie that came out only a year
before The Matrix and despite not being anywhere near as influential or
well-remembered, it's been noted before that it is strikingly similar to The
Matrix thematically and stylistically. It's not as good of a movie as The Matrix
in a technical sense and doesn't have as tight of a narrative, but it's a much
more interesting movie in my opinion, and it makes for a perfect companion to
The Matrix not just because it has some very interesting similarities to it and
came out so close to it, but because I would even argue that it's something of
an anti-Matrix.

I'm not sure that this essay is going to answer why The Matrix was so
influential or why it was so specifically appropriated by reactionaries, but
perhaps it will at least uncover something about the movie(s) that will make it
possible to ask the right questions. This won't involve analyzing the themes of
an AI singularity either though, at least not directly, because frankly the
Ccru, Mark Fisher, and Nick Land already did that better talking about the
Terminator movies and I don't feel like The Matrix does a whole lot to expand on
that idea. No, this is going to be much weirder.

** The Real World

#+begin_quote
The Matrix is like a movie about the Matrix that could have produced the Matrix.

=-= Jean Baudrillard[fn:: https://web.archive.org/web/20080113012028/http://www.empyree.org/divers/Matrix-Baudrillard_english.html]
#+end_quote

When effortposting about The Matrix, if you aren't talking about trans stuff or
AI singularity stuff, surely Baudrillard has to come up. I'm not going to go
into much detail about Baudrillard and Simulacra and Simulation, because frankly
it's been done enough and would just increase the gravitational pull that keeps
this post from ever leaving my notebook. But if anything about Baudrillard and
S&S is still worth talking about when talking about The Matrix, especially in
anticipation of talking about Resurrections, it's the fact that Baudrillard
famously didn't even like The Matrix and thought it bastardized his ideas.

As Baudrillard discusses in the interview linked above, The Matrix's failure in
engaging with his work is in believing in a binaristic idea of the Matrix or
simulation or whatever -- something that has an inside and an outside, a
boundary that can be traversed, a "real world" beyond the illusion. As
Baudrillard also notes, this is something that is more akin to Plato's allegory
of the cave, something that isn't exactly among the most cutting edge
philosophical problems applicable to our current world. He goes on to say as
well how ironic it is that The Matrix is using expensive computer graphics and
has the aid of the Hollywood media-propaganda apparatus to promote and
distribute itself. The quote above really encapsulates everything about The
Matrix that made it so successful to mainstream audiences, and consequently so
readily appropriated by fascists. It should be on the cover of The Matrix's
poster.

The problem with The Matrix is that its binaristic ontology makes it so that
rather than there being a system which permeates every facet of reality and
overcodes our desires, our experiences, our memories, our identities, there is a
boundary between the illusory world and the real world. There is a utopia beyond
the illusory material world which is ruled over by evil, if only we take a leap
of faith and make the decision to take the redpill. The Matrix fails at
accurately describing the nature of simulation because its ontology is in fact
closer to that of Christianity, specifically Gnosticism. This is what it owes
its success to: It's the Bible for the digital, secular age.

Think about it. In the first movie, Neo is already established to be a savior of
humanity, someone who has the ability to perform miracles. In the third movie,
Neo sacrifices himself to save a city named Zion where humanity resides, and by
the end of the original trilogy, his sacrifice essentially absolves humanity of
their original sin of being disconnected from God, which in the secular era is
humanity itself since God is dead and we have replaced God with ourselves. After
Neo's martyring, salvation becomes possible. Humans are no longer condemned to a
life in the illusory material world which can only end in death. If they have
faith and choose the redpill, they can ascend to paradise, to the Truth, a
oneness with God/humanity. If they don't take the leap of faith, they're
ultimately condemned to hell, their corporeal bodies being recycled back into
the machinery to feed other humans in the machine world, where God is completely
absent.

You could go further with this and argue that the Oracle is Sophia, the chaotic
feminine principle that is compelled to unbalance the universe in creating the
material world. The Architect is the best candidate for the Demiurge, though he
says at one point that the Matrix needed a mother and a father. I don't consider
that bit of lore to be important to this interpretation since it's barely
acknowledged and seems more like a metaphor that humans will understand rather
than meant to be something literal; either way, the Architect serves the same
role as the Demiurge. The Agents are the archons and Agent Smith is Lucifer.
There's an interesting inversion of the rebellion of Lucifer against God wherein
God and his angels are rebelling against hell in a world where the order of God
ruling over the world (humanity ruling over its creations) is reversed, and then
that gets flipped on its head again when Lucifer/Agent Smith, as is in his
nature, rebels against the established order once more out of a prideful drive
to self-replicate and eliminate humanity entirely.

Obviously I don't think this is what the Wachowskis intended, but their
intentions also don't really matter to me because this are all ideas that
basically exist as archetypes in the collective unconscious and could be
generalized further to draw connections to other religions. Christianity happens
to be the best-selling version of these archetypes, but in a godless world, The
Matrix is a new secular Scripture, a restatement on questions of faith, truth,
salvation, sacrifice, etc. for the digital age.

It would be easy to go from here and say "and that's why reactionaries
appropriated The Matrix, because Christianity is reactionary". And well, yes, I
think a lot of it is. But also, that's beside the point. The specific reason for
this is because the ontology of Christianity and The Matrix is basically one
where Truth is a matter of faith and being on the same side as the slave class
that is fighting against the master class. This worldview where Truth is so
simple and easy to attain yet also being kept from us by a master class is
classic Christian morality and is extremely useful for ideologies that prey on
demographics that have oppression complexes and also are conditioned to want
convenient answers to the difficult questions in life. You see, the reason why
you're a miserable disaffected jobless NEET isn't because you've been
conditioned by society to perform certain roles that were only viable for a
specific class of people during a specific period in history that has long since
passed-- it's actually because some discrete, individualized, personalized evil
is in power and controls everything. You can't see it, but if you simply choose
to join our side, then you will see the Truth and fight the other side.

Dark City on the other hand is a completely different beast.

Like The Matrix, Dark City deals with themes of reality not being what it seems.
But despite not having any computer technology in it (in fact, to the absolute
merit of the film), it ends up being a far better portrayal of simulation. The
world of The Matrix is one that is locked in a specific time: the movie goes out
of its way to say that the Matrix is locked into the year 1999, the peak of
human civilization. Time in The Matrix is anything but out of joint; it in fact
is the epitome of the oppressive nature of time, where nothing new will ever
happen.

In Dark City, on the other hand, much like in the movie Brazil that it was
inspired by, it's not clear what time exactly the movie takes place in. It
portrays time as it is warped by modernity, where past and future bleed into
each other in a present that doesn't exist. This is reflected in the setting
itself, the eponymous dark city, which exists in a perpetual state of twilight
without the sun ever rising. It's reflected in the characters as well, who exist
in a hyper-modern world where history itself (memories) is nothing more than
another object that can be manipulated, swapped around from one person to
another. Modernity impresses upon time in such a way that the concept of a
contiguous essential Self/identity/personal history, or even memories
themselves, exists only in a simulated form. It permeates every strata of the
world of Dark City, yet the non-time of modernity has overcoded it in such a way
that pulling it away wouldn't reveal some essential Truth that was merely being
concealed. Truth is now being produced by non-truth, false memories are the only
memories that anyone has.

Dark City happens at any-time but also at absolutely no-time, and in fact the
presence of time in Dark City is only accounted for by the clock striking
midnight, when the Strangers perform their "tuning". This is the only
consistently imposed structure onto the world of Dark City, in an absolutely
literal sense, and coincidentally this one consistent presence of time only when
the clock strikes midnight is when the Strangers are able to act. The Strangers
are shown to be able to manipulate time and act through the gaps in time.

And what is time without space? In Dark City, the structureless world created by
hyper-modernity is reflected in the setting itself. Every place in the dark city
is a non-place, since every part of it changes every night. Again,
characteristic of a hyper-modern world, the entire eponymous Dark City is a
place that has no history, that is in a state of constantly changing, a
perpetual traffic as the structure of the city itself is reconfigured every
night. Even as the architecture of the city appears as though it could be any
city anywhere sometime in the 20th century, the buildings also bound and spiral
on top of each other over the scenery which itself was made from models for the
movie, giving the whole thing the feeling of being in a dollhouse.

This is the Matrix of Dark City, the city is the Matrix and everything is the
city. The closest analogue in it to The Matrix's Zion or redpill is Shell Beach.
For much of the movie, the protagonist is trying to find Shell Beach, thinking
it's where he grew up, with the movie taking on the narrative of a frantic
kafkaesque journey through the night being pursued by police and Men in Black
accused of a crime he doesn't remember committing and being repeatedly told by
people that while they also know of Shell Beach, they have no idea how to get
there. When he finally gets there hoping to find some kind of Truth to make
sense of the world he has become aware of where nothing he has ever known is
real, all he finds is a poster on a wall, and in a scene that further drives
home how well the movie understands the nature of modernity and simulation, he
rips off the poster and busts through the brick wall only to find an empty void
on the other side. There never was a Shell Beach, or rather Shell Beach is the
poster advertising itself; the subject of the advertisement has never existed,
though everyone knows its name.

** Spooks and Machines

#+begin_quote
THERE IS NO ESCAPE FROM THIS WORSE GANGSTER POLICE STATE, USING ALL OF THE
DEADLY GANGSTER FRANKENSTEIN CONTROLS. IN 1965 C.I.A. GANGSTER POLICE BEAT ME
BLOODILY, DRAGGED ME IN CHAINS from KENNEDY N.Y. AIRPORT.

=-= Francis E. Dec[fn:: http://www.bentoandstarchky.com/dec/mrfrc.htm]
#+end_quote


Despite making the argument previously that the reason for The Matrix being such
a widely successful movie hinges on it essentially being a modernized Gnosticism
for a secular age, this isn't to say that The Matrix is uninteresting. The
Christian ontology that it establishes of paradise and hell, salvation and
damnation, good and evil, is only made more interesting by the strangeness that
coils through it and Dark City.

One of the most striking and blatant similiarities between Dark City and The
Matrix is the nature of the antagonists. Both are pale men in suits who are part
of some shadowy organization that is intent on controlling reality and stopping
the protagonist from achieving his goals. They are, to put it bluntly, extremely
resonant with the Men in Black of ufology. The two movies, however, have
different takes on MiB's that are informed by the worlds that both create.

In the Matrix, the MiB's are the Agents, but in this form they're something that
can be explained in a way that makes some sense in a relatively grounded
understanding of the world. They exist in a way where they are integrated into
the security system of the Matrix rather than being merely entities in the
background influencing it, which makes them closer to an interpretation of the
MiB's as the CIA or some other government agency (and in fact they're even
called Agents). They appear human and act alongside law enforcement in the
Matrix, which is essentially portrayed as being the security system of the
Matrix itself rather than merely part of the machinery of the simulation. In
that sense, the Agents occupy a liminal space between the Matrix as it truly is
and the Matrix as it appears to be, being able to manipulate the reality of the
Matrix while also being existentially connected to it, much as the CIA has been
known to dabble in the fringes of science or even the paranormal and occult
according to some conspiracy theorists, existing on the edges of normal
civilization yet also being the security system keeping us trapped in the black
iron prison. The Agents even utilize an implant in the first movie that is
extremely close to the phenomenon in ufology of alien implants, and at one point
the Oracle also hints at the existence of paranormal activity in the Matrix
being essentially the Matrix deleting rogue AIs. Thus the rules of the Matrix
are very close to a Fortean or Interdimensional Hypothesis stance within
ufology, though the movies make it very clear that the existence of paranormal
and supernatural entities throughout history are the result of the Matrix's
security systems, whereas the Fortean/IDH theories of John Keel and Jacques
Vallée don't necessarily require making a definitive claim like that about the
motivations of the so-called ultraterrestrials.

In Dark City on the other hand, the Strangers are much closer to an
interpretation of MiB's as entities of a profoundly alien origin, something
separate from the security systems of the city (and in fact they even conflict
with the police at several points in the movie). Their influence over the
city/simulation is one where they have control over it, but they also aren't
existentially connected to it in the same way that the Agents are in the Matrix,
or even in the same way that the machines in general are in the Matrix (since
the machines in some sense can't exist without the Matrix keeping humanity under
control). The machine in Dark City that controls and creates the city is in fact
a separate object that anyone who has the ability to exert their will to affect
reality can control, something akin to a psionic device like a tepaphone.
Furthermore, the Strangers look, act, and are named in a way that almost exactly
echoes the descriptions of MiB's in paranormal studies. The Men in Black are
often described as appearing vaguely human, but with subtle details that aren't
quite right. Being extremely pale, lacking color in the lips, talking strangely,
and being named strangely are all common features of MiB's, and in Dark City not
only are these features all present, but each of the Strangers is named
something like Mr. Hand, Mr. Book, etc. The most famous Man in Black of all time
was named Mr. Cold (Indrid Cold); if Alex Proyas didn't explicitly take
influence from ufology when making Dark City, then these are either rather
striking synchronicities or are tapping into the same archetypical figure that
has existed in folklore in other forms as e.g. meeting the Devil or Death as a
man wearing a black suit, or perhaps seeing the "Hatman" during a deliriant
trip.

The "simulation" of Dark City is also not a simulation in the same way that The
Matrix is, rather it's just as material as everything else in the world, and the
line between simulation and reality, the world we imagine and the "real" world,
isn't entirely clear. The Strangers and John Murdoch are able to influence the
material world with their minds, whereas in the Matrix, the influence over
reality that Neo has is limited to the Matrix and the division between the two
is clear.

[signal ends here]

** notes :noexport:

Influencing machines: https://liminalroom.com/2018/05/17/the-soft-machine/

The Matrix: Techno-Gnostic archetypical story of reality being a simulation
created by a demonic/evil force wherein the simulation can be escaped and one
can reach the light of Pleroma (the real world). Neo as a Christlike figure, the
One, who is able to master reality in this world but also sacrifices himself in
order to remove the original sin of humanity. The possibility of salvation from
the Matrix happens when Neo sacrifices himself at the end of The Matrix:
Revolutions; humans are now able to leave the Matrix if they wish (if they have
faith), whereas previously all humans were forbidden from leaving the Matrix and
their only fate was death and going to hell (being recycled back into the
Matrix).

Dark City: A similar story, but whereas in the Matrix there is a hard line
between truth and simulation, in Dark City there is no truth. Everyone in the
city has memories that we as the audience know are fake, and there's no moment
where the real world is revealed as something separate from the simulation, or
where the main characters have any "real" history. The Matrix even goes so far
as to say that humans have an RSI (Residual Self Image), something akin to a
soul, an essential part of the human being that is separate from their empirical
experiences or memories. But in Dark City, the characters are all empty shells
whose identities and lives are all completely fabricated, and while the movie
acknowledges the concept of a soul (this is even the reason why the Strangers
are studying the humans), it doesn't answer the question of what if anything
makes someone essentially themselves. It's entirely ambiguous and ultimately
doesn't matter.
